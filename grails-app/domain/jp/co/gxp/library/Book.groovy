package jp.co.gxp.library

class Book {

    String author

    String title

    static constraints = {
    }

    public String toString() {
        return "Book{" +
                "author='" + author + '\'' +
                ", title='" + title + '\'' +
                '}';
    }
}
